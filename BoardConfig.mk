USE_CAMERA_STUB := true

# inherit from the proprietary version
-include vendor/tcl/idol347/BoardConfigVendor.mk

TARGET_ARCH := arm
TARGET_NO_BOOTLOADER := true
TARGET_BOARD_PLATFORM := msm8916
TARGET_CPU_ABI := armeabi-v7a
TARGET_CPU_ABI2 := armeabi
TARGET_ARCH_VARIANT := armv7-a-neon
TARGET_CPU_VARIANT := cortex-a53
TARGET_CPU_SMP := true
ARCH_ARM_HAVE_TLS_REGISTER := true

TARGET_BOOTLOADER_BOARD_NAME := MSM8916
BOARD_CUSTOM_BOOTIMG_MK := device/tcl/idol347/mkbootimg.mk
TARGET_PREBUILT_DT_IMAGE := device/tcl/idol347/dt.img
BOARD_KERNEL_CMDLINE := console=ttyHSL0,115200,n8 androidboot.console=ttyHSL0 androidboot.hardware=qcom user_debug=30 msm_rtb.filter=0x3F ehci-hcd.park=3 androidboot.bootdevice=7824900.sdhci lpm_levels.sleep_disabled=1 androidboot.bootloader=L1AB2020BQ00 androidboot.selinux=permissive 
BOARD_KERNEL_BASE := 0x80000000
BOARD_KERNEL_PAGESIZE := 2048

# fix this up by examining /proc/mtd on a running device
BOARD_BOOTIMAGE_PARTITION_SIZE := 0x02000000
BOARD_RECOVERYIMAGE_PARTITION_SIZE := 0x02000000
BOARD_SYSTEMIMAGE_PARTITION_SIZE := 0x73333800
# USERDATA is for the default size of the partition - be careful 
BOARD_USERDATAIMAGE_PARTITION_SIZE := 0x139661c00
BOARD_FLASH_BLOCK_SIZE := 131072

TARGET_PREBUILT_KERNEL := device/tcl/idol347/kernel

BOARD_HAS_NO_SELECT_BUTTON := true

# Twrp 

DEVICE_RESOLUTION := 720x1280
#RECOVERY_GRAPHICS_USE_LINELENGTH := true

# disable the lock screen function
TW_NO_SCREEN_TIMEOUT := true 
# TW_USE_TOOLBOX := true
TARGET_RECOVERY_QCOM_RTC_FIX := true
